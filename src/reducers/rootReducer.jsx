import { combineReducers } from 'redux';

import dataRetrieval from './dataRetrieval';

export default combineReducers({
    dataRetrieval,
});
