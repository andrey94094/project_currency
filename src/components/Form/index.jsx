import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import "./Form.scss"
import { dataRetrievals } from '../../actionCreators'
import Button from '../Button'

class Form extends PureComponent {
  state = {
    dataRetrieval: [],
    isShowMore: false,
  };

  handleClick = async () => {
    this.setState({ isShowMore: !this.state.isShowMore });
    try {
      const response = await fetch('https://www.nbrb.by/api/exrates/rates?periodicity=0');
      const dataset = await response.json();
      const { dataRetrievals } = this.props;
      dataRetrievals(dataset);
    } catch (err) {
    }
  }

  render() {
    const { dataRetrieval } = this.props;
    const { isShowMore } = this.state;
    return (
      <section className="form">
        <Link to="/setting">Настройки</Link>
        <Button text={isShowMore ? "Обновить" : "Показать"} onClick={this.handleClick} />
        <table border="1" className="dataset">
          <tr>
            <th>Дата</th>
            <th>Буквенный код валюты</th>
            <th>Текущий курс валют</th>
          </tr>
          {dataRetrieval.map((item) => (
            <tr>
              <td>{item.Date}</td>
              <td>{item.Cur_Abbreviation}</td>
              <td>{item.Cur_OfficialRate}</td>
              <button onClick={() => navigator.clipboard.writeText(item.Cur_OfficialRate)}>Копировать курс {item.Cur_Abbreviation} в буффер обмена</button>
            </tr>
          ))}
        </table>

      </section>
    )
  }
}
const mapStateToProps = ({ dataRetrieval }) => ({
  dataRetrieval: dataRetrieval.dataRetrieval,
});
const mapDispatchToProps = (dispatch) => ({
  dataRetrievals: (dataset) => dispatch(dataRetrievals(dataset)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Form);
