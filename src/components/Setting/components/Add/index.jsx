import { connect } from 'react-redux';
import { PureComponent } from 'react';

import Input from "../Input";
import Button from "../../../Button"
import { addCurrenty } from '../../../../actionCreators';

class Add extends PureComponent {
  state = {
    Cur_Abbreviation: '',
    Cur_OfficialRate: '',
    Date: ''
  };

  handleClick = () => {
    const { addCurrenty } = this.props;
    const { Cur_Abbreviation, Cur_OfficialRate, Date } = this.state;
    const { currenty } = this.props;
    const currentys = {
      Cur_Abbreviation,
      Cur_OfficialRate,
      Date,
    };
    addCurrenty(currentys);
  }

  handleChangeCur_Abbreviation = (e) => {
    this.setState({ Cur_Abbreviation: e.target.value })
  };

  handleChangeDate = (e) => {
    this.setState({ Date: e.target.value })
  };

  handleChangeCur_OfficialRate = (e) => {
    this.setState({ Cur_OfficialRate: e.target.value })
  };

  render() {
    const { Cur_Abbreviation, Cur_OfficialRate, Date } = this.state;
    return (
      <div>
        <Input placeholder="Буквенный код" onChange={this.handleChangeCur_Abbreviation} value={Cur_Abbreviation} />
        <Input placeholder="Дата" onChange={this.handleChangeDate} value={Date} />
        <Input placeholder="Текущий курс валют" onChange={this.handleChangeCur_OfficialRate} value={Cur_OfficialRate} />
        <Button text="добавить" onClick={this.handleClick} />
      </div>
    );
  }
}

const mapStateToProps = ({ currentys }) => ({
  currentys: currentys,
});

const mapDispatchToProps = (dispatch) => ({
  addCurrenty: (currentys) => dispatch(addCurrenty(currentys)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Add);
